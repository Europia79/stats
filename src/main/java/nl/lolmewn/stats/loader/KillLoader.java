package nl.lolmewn.stats.loader;

import java.sql.ResultSet;
import java.sql.SQLException;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.loader.DataLoader;
import nl.lolmewn.stats.player.StatsPlayer;

/**
 *
 * @author Lolmewn
 */
public class KillLoader extends DataLoader {

    public KillLoader(StatsAPI api) {
        super(api);
    }

    @Override
    public boolean load(StatsPlayer player, Stat stat, ResultSet set) throws SQLException {
        player.getStatData(stat, set.getString("world"), true).setCurrentValue(new Object[]{set.getString("type")}, set.getInt("amount"));
        return true;
    }

}
