package nl.lolmewn.stats.api.loader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;

/**
 *
 * @author Lolmewn
 */
public class DefaultDataLoader extends DataLoader {

    public DefaultDataLoader(StatsAPI api) {
        super(api);
    }

    @Override
    public boolean load(StatsPlayer player, Stat stat, ResultSet set) throws SQLException {
        if (!this.hasColumn(set, stat.getName())) {
            return false;
        }
        StatData sd = player.getStatData(stat, set.getString("world"), true);
        switch (stat.getMySQLType()) {
            case BOOLEAN:
                sd.setCurrentValue(new Object[]{}, set.getBoolean(stat.getName().toLowerCase().replace(" ", "")) == true ? 1 : 0);
                break;
            case DOUBLE:
            case INTEGER:
                sd.setCurrentValue(new Object[]{}, set.getDouble(stat.getName().toLowerCase().replace(" ", "")));
                break;
            case TIMESTAMP:
                Timestamp stamp = set.getTimestamp(stat.getName().toLowerCase().replace(" ", ""));
                if (stamp != null) {
                    sd.setCurrentValue(new Object[]{}, stamp.getTime());
                } else {
                    return false;
                }
            default:
                return false; //string, idk but that's not a long value
        }
        return true;
    }

}
