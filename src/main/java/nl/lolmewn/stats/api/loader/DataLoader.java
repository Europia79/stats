package nl.lolmewn.stats.api.loader;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.player.StatsPlayer;

/**
 *
 * @author Lolmewn
 */
public abstract class DataLoader {

    private final StatsAPI api;

    public DataLoader(StatsAPI api) {
        this.api = api;
    }

    public StatsAPI getAPI() {
        return api;
    }

    public boolean hasColumn(ResultSet set, String columnName) throws SQLException {
        ResultSetMetaData rsmd = set.getMetaData();
        int columns = rsmd.getColumnCount();
        for (int x = 1; x <= columns; x++) {
            if (columnName.toLowerCase().replace(" ", "").equals(rsmd.getColumnName(x))) {
                return true;
            }
        }
        return false;
    }

    public abstract boolean load(StatsPlayer player, Stat stat, ResultSet set) throws SQLException;

}
