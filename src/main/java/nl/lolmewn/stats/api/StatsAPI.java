/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.loader.DataLoader;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatTableType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.api.mysql.StatsTableManager;
import nl.lolmewn.stats.api.saver.DataSaver;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import nl.lolmewn.stats.signs.SignManager;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class StatsAPI {

    private final Main plugin;

    public StatsAPI(Main plugin) {
        this.plugin = plugin;
    }

    /**
     * @param name - Player name
     * @param world World name
     * @return playtime in seconds by <player> in world <world>
     */
    public double getPlaytime(String name, String world) {
        StatsPlayer player = getPlayer(name);
        return player.getStatData(this.getStat("Playtime"), world, true).getValue(new Object[]{});
    }

    /**
     * @param name - Player name
     * @return playtime in seconds by <player> in all worlds combined
     */
    public double getPlaytime(String name) {
        StatsPlayer player = getPlayer(name);
        return player.getGlobalStatData(this.getStat("Playtime")).getValue(new Object[]{});
    }

    /**
     * @param world World name
     * @param name Player name
     * @return total blocks broken by <player> in world <world>
     */
    public double getTotalBlocksBroken(String name, String world) {
        StatsPlayer player = getPlayer(name);
        StatData stat = player.getStatData(this.getStat("Block break"), world, false);
        if (stat == null) {
            return 0;
        }
        double value = 0;
        for (Object[] vars : stat.getAllVariables()) {
            value += stat.getValue(vars);
        }
        return value;
    }

    /**
     * @param name Player name
     * @return total blocks broken by <player> in all worlds combined
     */
    public double getTotalBlocksBroken(String name) {
        StatsPlayer player = getPlayer(name);
        StatData stat = player.getGlobalStatData(this.getStat("Block break"));
        if (stat == null) {
            return 0;
        }
        double value = 0;
        for (Object[] vars : stat.getAllVariables()) {
            value += stat.getValue(vars);
        }
        return value;
    }

    /**
     * @param name - Player name
     * @param world World name
     * @return total blocks placed by <player> in world <world>
     */
    public double getTotalBlocksPlaced(String name, String world) {
        StatsPlayer player = getPlayer(name);
        StatData stat = player.getStatData(this.getStat("Block place"), world, false);
        if (stat == null) {
            return 0;
        }
        double value = 0;
        for (Object[] vars : stat.getAllVariables()) {
            value += stat.getValue(vars);
        }
        return value;
    }

    /**
     * @param name Player name
     * @return total blocks placed by <player> in all worlds combined
     */
    public double getTotalBlocksPlaced(String name) {
        StatsPlayer player = getPlayer(name);
        StatData stat = player.getGlobalStatData(this.getStat("Block place"));
        if (stat == null) {
            return 0;
        }
        double value = 0;
        for (Object[] vars : stat.getAllVariables()) {
            value += stat.getValue(vars);
        }
        return value;
    }

    /**
     * @param name - Player name
     * @param world World name
     * @return total deaths by <player> in world <world>
     */
    public double getTotalDeaths(String name, String world) {
        StatsPlayer player = getPlayer(name);
        StatData stat = player.getStatData(this.getStat("Death"), world, true);
        if (stat == null) {
            return 0;
        }
        double value = 0;
        for (Object[] vars : stat.getAllVariables()) {
            value += stat.getValue(vars);
        }
        return value;
    }

    /**
     * @param name - Player name
     * @return total deaths by <player> in all worlds combined
     */
    public double getTotalDeaths(String name) {
        StatsPlayer player = getPlayer(name);
        StatData stat = player.getGlobalStatData(this.getStat("Death"));
        if (stat == null) {
            return 0;
        }
        double value = 0;
        for (Object[] vars : stat.getAllVariables()) {
            value += stat.getValue(vars);
        }
        return value;
    }

    /**
     * @param name - Player name
     * @param world World name
     * @return total kills by <player> in world <world>
     */
    public double getTotalKills(String name, String world) {
        StatsPlayer player = getPlayer(name);
        StatData stat = player.getStatData(this.getStat("Kill"), world, true);
        if (stat == null) {
            return 0;
        }
        double value = 0;
        for (Object[] vars : stat.getAllVariables()) {
            value += stat.getValue(vars);
        }
        return value;
    }

    /**
     * @param name - Player name
     * @return total kills by <player> in all worlds combined
     */
    public double getTotalKills(String name) {
        StatsPlayer player = getPlayer(name);
        StatData stat = player.getGlobalStatData(this.getStat("Kill"));
        if (stat == null) {
            return 0;
        }
        double value = 0;
        for (Object[] vars : stat.getAllVariables()) {
            value += stat.getValue(vars);
        }
        return value;
    }

    /**
     * @param player Player name
     * @param world World name
     * @return total blocks placed + total blocks broken by <player> in world
     */
    public double getTotalBlocks(String player, String world) {
        return this.getTotalBlocksBroken(player, world) + this.getTotalBlocksPlaced(player, world);
    }

    /**
     * @param player Player name
     * @return total blocks placed + total blocks broken by <player> in all
     * worlds combined
     * <world>
     */
    public double getTotalBlocks(String player) {
        return this.getTotalBlocksBroken(player) + this.getTotalBlocksPlaced(player);
    }

    /**
     * All stats tables are prefixed with this. The player table, for example,
     * is <code>getDatabasePrefix() + "player"</code> If default prefix is used,
     * this would output Stats_player
     *
     * @return Prefix for tables
     */
    public String getDatabasePrefix() {
        return this.plugin.getSettings().getDbPrefix();
    }

    /**
     * The signmanager manages all things sign from Stats.
     *
     * @return SignManager instance
     */
    public SignManager getSignManager() {
        return plugin.getSignManager();
    }

    /**
     * Retrieves and returns a connection from the pool. Don't forget to close
     * it afterwards!
     *
     * @return Connection from the pool
     */
    public Connection getConnection() {
        return plugin.getMySQL().getConnection();
    }

    /**
     * Check if the block at the given location is a stats sign.
     *
     * @param loc - Location to check at
     * @return true if it is a Stats Sign, false otherwise
     */
    public boolean isStatsSign(Location loc) {
        return plugin.getSignManager().getSignAt(loc) != null;
    }

    /**
     * Tells you if the StatType is enabled or disabled through the config.
     *
     * @param type - The StatType to look up
     * @return true if stat is enabled
     */
    public boolean isStatEnabled(String type) {
        return !plugin.getSettings().getDisabledStats().contains(type);
    }

    public Stat getStat(String statName) {
        return plugin.getStatTypes().find(statName);
    }

    public Collection<Stat> getAllStats() {
        return plugin.getStatTypes().values();
    }

    public Stat getStatExact(String exactStatName) {
        return plugin.getStatTypes().get(exactStatName);
    }

    /**
     * Gets or creates a new player. If one does not exist yet (or is not
     * loaded), one will be loaded asynchronous. Once loading is complete, it
     * will synchronize with the StatsPlayer this method returns.
     *
     * @param player Player object
     * @return StatsPlayer object, empty if loading.
     */
    public StatsPlayer getPlayer(Player player) {
        return this.plugin.getPlayerManager().getPlayer(player);
    }

    /**
     * Gets or creates a new player. If one does not exist yet (or is not
     * loaded), one will be loaded asynchronous. Once loading is complete, it
     * will synchronize with the StatsPlayer this method returns.
     *
     * @param name Player name
     * @return StatsPlayer object, empty if loading.
     */
    public StatsPlayer getPlayer(String name) {
        return this.getPlayer(name, true);
    }

    /**
     * Gets or creates a new player. If one does not exist yet (or is not
     * loaded), one will be loaded asynchronous. Once loading is complete, it
     * will synchronize with the StatsPlayer this method returns.
     *
     * @param uuid Player UUID
     * @return StatsPlayer object, empty if loading.
     */
    //public StatsPlayer getPlayer(UUID uuid) {
    //    return this.plugin.getPlayerManager().getPlayer(plugin.getServer().getOfflinePlayer(uuid));
    //}

    /**
     * Gets or creates a new player. If one does not exist yet (or is not
     * loaded), one will be loaded asynchronous. Once loading is complete, it
     * will synchronize with the StatsPlayer this method returns.
     *
     * @param id StatsPlayer unique ID
     * @return StatsPlayer object, empty if loading.
     */
    public StatsPlayer getPlayer(int id) {
        for (StatsPlayer player : this.plugin.getPlayerManager().getPlayers()) {
            if (player.getId() == id) {
                return player;
            }
        }
        return null;
    }

    /**
     * Gets or creates a new player. If one does not exist yet (or is not
     * loaded), one will be loaded asynchronous. Once loading is complete, it
     * will synchronize with the StatsPlayer this method returns.
     *
     * @param name Player name
     * @param createIfNotExists create a new StatsPlayer object if one does not
     * exist. If false and no StatsPlayer exists, returns null.
     * @return StatsPlayer object, empty if loading.
     */
    @Deprecated
    public StatsPlayer getPlayer(String name, boolean createIfNotExists) {
        if (createIfNotExists) {
            return plugin.getPlayerManager().getPlayer(plugin.getServer().getOfflinePlayer(name));
        }
        return plugin.getPlayerManager().findPlayer(name);
    }

    /**
     * Gets or creates a new player. If one does not exist yet (or is not
     * loaded), one will be loaded asynchronous. Once loading is complete, it
     * will synchronize with the StatsPlayer this method returns.
     *
     * @param op Player to look up
     * @param createIfNotExists create a new StatsPlayer object if one does not
     * exist. If false and no StatsPlayer exists, returns null.
     * @return StatsPlayer object, empty if loading.
     */
    public StatsPlayer getPlayer(OfflinePlayer op, boolean createIfNotExists) {
        if (createIfNotExists) {
            return plugin.getPlayerManager().getPlayer(op);
        }
        return plugin.getPlayerManager().findPlayer(op.getName());
    }
    
    public StatsPlayer getPlayer(UUID uuid){
        return getPlayer(uuid, true);
    }
    
    public StatsPlayer getPlayer(UUID uuid, boolean createIfNotExists){
        return this.getPlayer(plugin.getServer().getOfflinePlayer(uuid), true);
    }
    
    public StatsPlayer getPlayer(OfflinePlayer player){
        return plugin.getPlayerManager().getPlayer(player);
    }

    public Stat addStat(String statName, StatDataType type, StatTableType tableType, StatsTable table, MySQLType mysqlType, DataLoader loader, DataSaver dataSaver) {
        return plugin.getStatTypes().addStat(statName, type, tableType, table, mysqlType, loader, dataSaver);
    }

    public Stat addStat(String name, StatDataType dataType, StatTableType tableType, StatsTable table, MySQLType type, String valueColumn, String[] varColumns) {
        return plugin.getStatTypes().addStat(name, dataType, tableType, table, type, valueColumn, varColumns);
    }

    public boolean isUsingBetaFunctions() {
        return true;
    }

    public boolean isCreatingSnapshots() {
        return plugin.getSettings().createSnapshots();
    }

    public StatsTable getStatsTable(String name) {
        return plugin.getStatsTableManager().get(name);
    }

    public StatsTableManager getStatsTableManager() {
        return plugin.getStatsTableManager();
    }

    /**
     * Gets the ID of a player. If player is online, it'll use the StatsPlayer
     * object. If not, the database gets queried.
     *
     * If the player is not found at all, -1 is returned.
     *
     * @param name Playername
     * @return ID of player
     */
    public int getPlayerId(String name) {
        StatsPlayer player = this.getPlayer(name, false);
        if (player != null && !player.isTemp()) {
            return player.getId();
        }
        Connection con = this.getConnection();
        try {
            PreparedStatement st = con.prepareStatement("SELECT player_id FROM " + this.getDatabasePrefix() + "players WHERE name=?");
            st.setString(1, name);
            ResultSet set = st.executeQuery();
            if (set.next()) {
                int id = set.getInt("player_id");
                set.close();
                st.close();
                con.close();
                return id;
            }
            set.close();
            st.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(StatsAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int getPlayerId(UUID uuid) {
        StatsPlayer player = this.getPlayer(plugin.getServer().getOfflinePlayer(uuid), false);
        if (player != null && !player.isTemp()) {
            return player.getId();
        }
        Connection con = this.getConnection();
        try {
            PreparedStatement st = con.prepareStatement("SELECT player_id FROM " + this.getDatabasePrefix() + "players WHERE uuid=?");
            st.setString(1, uuid.toString());
            ResultSet set = st.executeQuery();
            if (set.next()) {
                int id = set.getInt("player_id");
                set.close();
                st.close();
                con.close();
                return id;
            }
            set.close();
            st.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(StatsAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public String getPlayerName(int id) {
        if (this.getPlayer(id) != null) {
            return this.getPlayer(id).getPlayername();
        }
        Connection con = this.getConnection();
        try {
            PreparedStatement st = con.prepareStatement("SELECT name FROM " + this.getDatabasePrefix() + "players WHERE player_id=?");
            st.setInt(1, id);
            ResultSet set = st.executeQuery();
            if (set.next()) {
                String name = set.getString("name");
                set.close();
                st.close();
                con.close();
                return name;
            }
            set.close();
            st.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(StatsAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean hasColumn(Connection con, String table, String column) throws SQLException {
        return this.plugin.getMySQL().hasColumn(con, table, column);
    }

}
