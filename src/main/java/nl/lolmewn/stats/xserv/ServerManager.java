package nl.lolmewn.stats.xserv;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;

/**
 * This class is only used by the host of all servers.
 *
 * @author Sybren
 */
public class ServerManager extends HashMap<Integer, Server> {

    private final Main m;
    private ServerSocket soc;

    public ServerManager(Main m) {
        this.m = m;
        this.startServerThread(m.getSettings().getXServerPort());
    }

    private void startServerThread(int xServerPort) {
        try {
            this.soc = new ServerSocket(xServerPort);
            m.getServer().getScheduler().runTaskAsynchronously(m, new Runnable() {

                @Override
                public void run() {
                    try {
                        Socket incoming = soc.accept();
                        Server server = new Server(m, incoming, size());
                        put(size(), server);
                        server.startComms();
                    } catch (IOException ex) {
                        Logger.getLogger(ServerManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        } catch (IOException ex) {
            Logger.getLogger(ServerManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
