package nl.lolmewn.stats.command;

import nl.lolmewn.stats.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Lolmewn
 */
public abstract class StatsSubCommand implements CommandExecutor {

    private final Main main;
    private final String perm;

    public StatsSubCommand(Main main, String perm) {
        this.main = main;
        this.perm = perm;
    }

    public Main getPlugin() {
        return main;
    }

    public abstract boolean execute(CommandSender sender, String[] args);

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (perm != null && !sender.hasPermission(perm)) {
            sender.sendMessage(ChatColor.RED + "You do not have permissions for this command!");
            return true;
        }
        return execute(sender, args);
    }

}
