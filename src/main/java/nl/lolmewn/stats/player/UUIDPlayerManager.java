/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.StatsPlayerLoadedEvent;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.signs.SignType;
import nl.lolmewn.stats.signs.StatsSign;
import org.apache.commons.lang.Validate;
import org.bukkit.OfflinePlayer;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class UUIDPlayerManager implements PlayerManager {

    private final Main plugin;
    private final ConcurrentHashMap<UUID, StatsPlayer> players = new ConcurrentHashMap<UUID, StatsPlayer>();

    public UUIDPlayerManager(Main m) {
        plugin = m;
    }

    @Override
    public boolean hasPlayer(OfflinePlayer player) {
        Validate.notNull(player);
        return this.players.containsKey(player.getUniqueId());
    }

    @Deprecated
    public boolean hasPlayer(String name) {
        return hasPlayer(plugin.getServer().getOfflinePlayer(name));
    }

    @Override
    public void addPlayer(OfflinePlayer player, StatsPlayer statsPlayer) {
        Validate.notNull(player);
        this.players.put(player.getUniqueId(), statsPlayer);
    }

    @Deprecated
    public void addPlayer(String name, StatsPlayer player) {
        this.addPlayer(plugin.getServer().getOfflinePlayer(name), player);
    }

    @Override
    public void unloadPlayer(OfflinePlayer player) {
        Validate.notNull(player);
        this.players.remove(player.getUniqueId());
    }

    @Deprecated
    public void unloadPlayer(String name) {
        this.unloadPlayer(plugin.getServer().getOfflinePlayer(name));
    }

    @Override
    public StatsPlayer getPlayer(OfflinePlayer player) {
        Validate.notNull(player);
        if (!this.hasPlayer(player)) {
            StatsPlayer sp = new StatsPlayer(plugin, player.getName(), true);
            this.players.put(player.getUniqueId(), sp);
            this.loadPlayer(player);
            return sp;
        }
        return players.get(player.getUniqueId());
    }

    @Override
    public Collection<StatsPlayer> getPlayers() {
        return this.players.values();
    }

    @Override
    public StatsPlayer findPlayer(String key) {
        Validate.notNull(key);
        for (StatsPlayer player : this.getPlayers()) {
            if (player.getPlayername().toLowerCase().startsWith(key.toLowerCase())) {
                return player;
            }
        }
        return null;
    }

    @Override
    public void loadPlayer(final OfflinePlayer player) {
        Validate.notNull(player);
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                StatsPlayer sp = new StatsPlayer(plugin, player.getName(), false);
                if (plugin.newConfig || plugin.getMySQL() == null) {
                    return;
                }
                plugin.debug("Starting to load " + player.getName() + " async...");
                try {
                    Connection con = plugin.getMySQL().getConnection();
                    PreparedStatement st = con.prepareStatement("SELECT * FROM " + plugin.getSettings().getDbPrefix() + "players WHERE uuid=? LIMIT 1"); //Change to UUID when 1.7 comes out
                    st.setString(1, player.getUniqueId().toString());
                    ResultSet set = st.executeQuery();
                    if (!set.next()) {
                        //There's no player yet with this uuid, maybe a player with the same name though?
                        PreparedStatement nameCheck = con.prepareStatement("SELECT * FROM " + plugin.getSettings().getDbPrefix() + "players WHERE name=? LIMIT 1");
                        nameCheck.setString(1, player.getName());
                        ResultSet back = nameCheck.executeQuery();
                        if (!back.next()) {
                            if (plugin.getServer().getPlayer(player.getUniqueId()) != null) {
                                //one really doesn't exist... lets add one
                                PreparedStatement insert = con.prepareStatement("INSERT INTO " + plugin.getSettings().getDbPrefix() + "players (name, uuid) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
                                insert.setString(1, player.getName());
                                insert.setString(2, player.getUniqueId().toString());
                                int affected = insert.executeUpdate();
                                if (affected == 0) {
                                    throw new SQLException("No user exists, but a new can't be created either. :O");
                                }
                                ResultSet keys = insert.getGeneratedKeys();
                                if (keys.next()) {
                                    sp.setId(keys.getInt(1));
                                }
                                keys.close();
                                insert.close();
                            }
                        } else {
                            PreparedStatement addUUID = con.prepareStatement("UPDATE " + plugin.getSettings().getDbPrefix() + "players SET uuid=? WHERE name=?");
                            addUUID.setString(1, player.getUniqueId().toString());
                            addUUID.setString(2, player.getName());
                            addUUID.execute();
                            addUUID.close();
                            sp.setId(back.getInt("player_id"));
                        }
                        back.close();
                        nameCheck.close();
                    } else {
                        sp.setId(set.getInt("player_id"));
                        if (!set.getString("name").equals(player.getName())) {
                            PreparedStatement updateName = con.prepareStatement("UPDATE " + plugin.getSettings().getDbPrefix() + "players SET name=? WHERE uuid=?");
                            updateName.setString(1, player.getName());
                            updateName.setString(2, player.getUniqueId().toString());
                            updateName.execute();
                            updateName.close();
                        }
                    }
                    set.close();
                    st.close();
                    for (StatsTable table : plugin.getStatsTableManager().values()) {
                        table.loadStats(con, sp);
                    }
                    con.close();
                } catch (SQLException e) {
                    Logger.getLogger(UUIDPlayerManager.class.getName()).log(Level.SEVERE, null, e);
                }

                if (players.containsKey(player.getUniqueId())) {
                    StatsPlayer old = players.get(player.getUniqueId());
                    boolean has = false;
                    for (String world : old.getWorlds()) {
                        if (old.hasStat(plugin.getStatTypes().get("Lastjoin"), world)) {
                            has = true;
                        }
                    }
                    if (!has && plugin.getServer().getPlayerExact(old.getPlayername()) == null) {
                        plugin.debug("Temp StatsPlayer object has no Lastjoin value, player is also not online - assuming this player doesn't exist (and therefore, deleting the object)");
                        players.remove(player.getUniqueId());
                        return;
                    }
                    players.put(player.getUniqueId(), sp);
                    sp.syncData(old);
                } else {
                    players.put(player.getUniqueId(), sp);
                }
                if (plugin.getSettings().isInstaUpdateSigns()) {
                    for (StatsSign sign : plugin.getSignManager().getAllSigns()) {
                        if (sign.getSignType().equals(SignType.PLAYER) && sign.getVariable().equals(Integer.toString(sp.getId()))) {
                            sp.addSignReference(sign, sign.getWorld());
                            sign.setAttachedToStat(true);
                        }
                    }
                }
                StatsPlayerLoadedEvent event = new StatsPlayerLoadedEvent(sp, true);
                plugin.getServer().getPluginManager().callEvent(event);
            }
        });
    }

    @Override
    public void unloadPlayer(StatsPlayer player) {
        this.unloadPlayer(player.getPlayername());
    }
}
