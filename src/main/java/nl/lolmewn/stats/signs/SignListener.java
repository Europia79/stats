/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.signs;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatUpdateEvent;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import nl.lolmewn.stats.signs.events.StatsSignCreateEvent;
import nl.lolmewn.stats.signs.events.StatsSignDestroyEvent;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class SignListener implements Listener {

    private final Main plugin;
    private final HashMap<String, Location> customSetters = new HashMap<String, Location>();
    private final HashMap<String, Location> signLineSettings = new HashMap<String, Location>();

    public SignListener(Main m) {
        this.plugin = m;
    }

    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        if (!event.getLine(0).equalsIgnoreCase("[Stats]")) {
            return;
        }
        if (!event.getPlayer().hasPermission("stats.sign.place")) {
            event.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to place a stats sign!");
            event.setCancelled(true);
            event.getBlock().setType(Material.AIR);
            event.getPlayer().getInventory().addItem(new ItemStack(Material.SIGN, 1));
            return;
        }
        SignType type;
        if (event.getLine(2).equalsIgnoreCase("global")) {
            type = SignType.GLOBAL;
        } else if (event.getLine(2).equalsIgnoreCase("Custom")) {
            type = SignType.CUSTOM;
        } else if (event.getLine(2).equalsIgnoreCase("rightclick")) {
            type = SignType.RIGHTCLICK;
        } else if (event.getLine(2).equalsIgnoreCase("player")) {
            if (event.getLine(3).equals("")) {
                event.setCancelled(true);
                event.getBlock().setType(Material.AIR);
                event.getPlayer().getInventory().addItem(new ItemStack(Material.SIGN, 1));
                event.getPlayer().sendMessage(ChatColor.RED + "Last line should be a player name");
                return;
            }
            type = SignType.PLAYER;
        } else {
            event.setCancelled(true);
            event.getBlock().setType(Material.AIR);
            event.getPlayer().getInventory().addItem(new ItemStack(Material.SIGN, 1));
            event.getPlayer().sendMessage(ChatColor.RED + "Only signs of type 'global', 'player', 'rightclick' or 'custom' allowed");
            return;
        }
        Stat stat = plugin.getStatTypes().find(event.getLine(1));
        if (stat == null) {
            event.setCancelled(true);
            event.getBlock().setType(Material.AIR);
            event.getPlayer().getInventory().addItem(new ItemStack(Material.SIGN, 1));
            event.getPlayer().sendMessage(ChatColor.RED + "StatType not recognized. Please use an appropriate one.");
            return;
        }
        StatsSign sign = new StatsSign(plugin.getAPI(),
                type, stat,
                event.getBlock().getWorld().getName(),
                event.getBlock().getX(),
                event.getBlock().getY(),
                event.getBlock().getZ());
        if (type == SignType.PLAYER) {
            sign.setVariable(event.getLine(3));
        }

        StatsSignCreateEvent createEvent = new StatsSignCreateEvent(sign, event.getPlayer());
        this.plugin.getServer().getPluginManager().callEvent(createEvent);
        if (createEvent.isCancelled()) {
            return;
        }

        if (type.equals(SignType.CUSTOM)) {
            event.getPlayer().sendMessage(ChatColor.GREEN + "Please say the SQL Query to get the data from in chat");
            this.customSetters.put(event.getPlayer().getName(), event.getBlock().getLocation());
            sign.setVariable("UNSET");
            sign.setSignLine("UNSET");
        }
        plugin.getSignManager().addSign(sign, event.getBlock().getLocation());
        event.getBlock().setMetadata("statssign", new FixedMetadataValue(this.plugin, true));

        event.setLine(1, ChatColor.YELLOW + "This sign is");
        event.setLine(2, ChatColor.YELLOW + "pending for");
        event.setLine(3, ChatColor.YELLOW + "data updates...");
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void chat(AsyncPlayerChatEvent event) {
        if (this.customSetters.containsKey(event.getPlayer().getName())) {
            try {
                Connection con = plugin.getMySQL().getConnection();
                Statement st = con.createStatement();
                st.executeQuery(event.getMessage());
                st.close();
                con.close();
                this.signLineSettings.put(event.getPlayer().getName(), this.customSetters.get(event.getPlayer().getName()));
                this.customSetters.remove(event.getPlayer().getName());
                event.setCancelled(true);
                StatsSign sign = plugin.getSignManager().getSignAt(this.signLineSettings.get(event.getPlayer().getName()));
                sign.setVariable(event.getMessage());
                sign.setSignLine(ChatColor.RED + "Placeholder");
                event.getPlayer().sendMessage(ChatColor.GREEN + "Please set line two for this sign now by saying it in chat.");
                return;
            } catch (SQLException ex) {
                Logger.getLogger(SignListener.class.getName()).log(Level.SEVERE, null, ex);
                this.plugin.getLogger().severe(ChatColor.RED + "The exeption above was generated by someone entering a faulty SQL query to the Stats plugin, please do NOT report it");
                event.getPlayer().sendMessage(ChatColor.RED + "Faulty SQL query, check the logs and try again.");
                event.setCancelled(true);
            }
        }
        if (this.signLineSettings.containsKey(event.getPlayer().getName())) {
            StatsSign sign = plugin.getSignManager().getSignAt(this.signLineSettings.get(event.getPlayer().getName()));
            sign.setSignLine(event.getMessage());
            event.setCancelled(true);
            event.getPlayer().sendMessage("Sign is now ready for use!");
            this.signLineSettings.remove(event.getPlayer().getName());
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        Location loc = event.getBlock().getLocation();
        if (this.plugin.getSignManager().getSignAt(loc) != null) {
            StatsSign sign = this.plugin.getSignManager().getSignAt(loc);
            StatsSignDestroyEvent ev = new StatsSignDestroyEvent(event.getPlayer(), sign);
            plugin.getServer().getPluginManager().callEvent(ev);
            if (ev.isCancelled()) {
                return;
            }
            if (sign.isAttachedToStat() && (sign.getSignType().equals(SignType.PLAYER) || sign.getSignType().equals(SignType.RIGHTCLICK))) {
                String player = sign.getVariable();
                int id = Integer.parseInt(player);
                StatsPlayer sPlayer = this.plugin.getAPI().getPlayer(id);
                if (sPlayer == null) {
                    //offline, whatever
                    return;
                } else {
                    sPlayer.getStatData(sign.getStat(), sign.getWorld(), true).removeSign(sign);
                }
            }
            plugin.getSignManager().removeSign(loc);
            event.getPlayer().sendMessage(ChatColor.GREEN + "Stats sign destroyed and deleted!");
        }
    }

    @EventHandler
    public void statsSignCreate(StatsSignCreateEvent event) {
        if (!event.getStatsSign().getSignType().equals(SignType.PLAYER)) {
            return;
        }
        StatsPlayer player = this.plugin.getPlayerManager().findPlayer(event.getStatsSign().getVariable());
        if (player == null) {
            return;
        }
        player.addSignReference(event.getStatsSign(), event.getCreator().getWorld().getName());
        event.getStatsSign().setAttachedToStat(true);
    }

    @EventHandler
    public void statsSignDestroy(StatsSignDestroyEvent event) {
        if (!event.getPlayer().hasPermission("stats.sign.destroy")) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to destroy a stats sign!");
        }
    }

    @EventHandler
    public void statUpdate(final StatUpdateEvent event) {
        if (!event.getStatData().hasSigns()) {
            return;
        }
        final HashMap<StatsSign, String[]> updates = new HashMap<StatsSign, String[]>();
        for (String signLoc : event.getStatData().getSignLocations()) {
            StatsSign sign = plugin.getSignManager().getSignAt(signLoc);
            if (sign != null) {

                double value = 0;
                
                StatData global = event.getStatData().getStatsPlayer().getGlobalStatData(event.getStat());

                for (Object[] vars : global.getAllVariables()) {
                    value += global.getValue(vars);
                }

                value += event.getUpdateValue();

                String line1 = ChatColor.BLACK + "[" + ChatColor.YELLOW + "Stats" + ChatColor.BLACK + "]";
                String line2 = sign.getSignLine().substring(0, 1).toUpperCase() + sign.getSignLine().substring(1).toLowerCase();
                String line3 = "by " + event.getPlayer().getPlayername();
                String line4;
                if (sign.getStat().getName().equals("Playtime")) {
                    long playTimeSeconds = (long) value;
                    line4 = String.format("%dd %dh %dm %ds",
                            TimeUnit.SECONDS.toDays(playTimeSeconds),
                            TimeUnit.SECONDS.toHours(playTimeSeconds) - TimeUnit.SECONDS.toDays(playTimeSeconds) * 24,
                            TimeUnit.SECONDS.toMinutes(playTimeSeconds) - TimeUnit.SECONDS.toHours(playTimeSeconds) * 60,
                            TimeUnit.SECONDS.toSeconds(playTimeSeconds) - TimeUnit.SECONDS.toMinutes(playTimeSeconds) * 60);
                } else if (sign.getStat().getName().equals("Move")) {
                    line4 = new DecimalFormat("###0.##").format(value);
                } else {
                    line4 = Double.toString(value);
                }
                if (line4.endsWith(".0")) {
                    line4 = line4.substring(0, line4.length() - 2);
                }
                updates.put(sign, new String[]{line1, line2, line3, line4});
            }
        }
        plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
            @Override
            public void run() {
                for (StatsSign sign : updates.keySet()) {
                    if (!sign.isActive()) {
                        continue;
                    }
                    String[] values = updates.get(sign);
                    sign.updateSign(values[0], values[1], values[2], values[3]);
                }
            }
        });
    }

    @EventHandler
    public void onRightClick(PlayerInteractEvent event) {
        if (!event.hasBlock()) {
            return;
        }
        Block clicked = event.getClickedBlock();
        StatsSign sign = this.plugin.getSignManager().getSignAt(clicked.getLocation());
        if (sign == null) {
            return;
        }
        if (!sign.getSignType().equals(SignType.RIGHTCLICK)) {
            return;
        }

        if (sign.hasVariable()) {
            StatsPlayer old = plugin.getAPI().getPlayer(Integer.parseInt(sign.getVariable()));
            if (old != null) {
                old.getStatData(sign.getStat(), sign.getWorld(), true).removeSign(sign);
            }
        }
        StatsPlayer ne = plugin.getAPI().getPlayer(event.getPlayer());
        ne.getStatData(sign.getStat(), sign.getWorld(), true).addSign(sign);
        StatData data = ne.getStatData(sign.getStat(), sign.getWorld(), true);
        sign.setVariable(Integer.toString(ne.getId()));
        double value = 0;

        for (Object[] vars : data.getAllVariables()) {
            value += data.getValue(vars);
        }

        String line1 = ChatColor.BLACK + "[" + ChatColor.YELLOW + "Stats" + ChatColor.BLACK + "]";
        String line2 = sign.getSignLine().substring(0, 1).toUpperCase() + sign.getSignLine().substring(1).toLowerCase();
        String line3 = "by " + event.getPlayer().getName();
        String line4;
        if (sign.getStat().getName().equals("Playtime")) {
            long playTimeSeconds = (long) value;
            line4 = String.format("%dd %dh %dm %ds",
                    TimeUnit.SECONDS.toDays(playTimeSeconds),
                    TimeUnit.SECONDS.toHours(playTimeSeconds) - TimeUnit.SECONDS.toDays(playTimeSeconds) * 24,
                    TimeUnit.SECONDS.toMinutes(playTimeSeconds) - TimeUnit.SECONDS.toHours(playTimeSeconds) * 60,
                    TimeUnit.SECONDS.toSeconds(playTimeSeconds) - TimeUnit.SECONDS.toMinutes(playTimeSeconds) * 60);
        } else if (sign.getStat().getName().equals("Move")) {
            line4 = new DecimalFormat("###0.##").format(value);
        } else {
            line4 = Double.toString(value);
        }
        if (line4.endsWith(".0")) {
            line4 = line4.substring(0, line4.length() - 2);
        }
        sign.updateSign(line1, line2, line3, line4);
    }
}
