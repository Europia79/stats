/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.signs;

import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class StatsSign {

    private StatsAPI api;
    private Stat stat;
    private String type;
    private SignType signType;
    private final String world;
    private final int x, y, z;
    private String variable;
    private String signLine;
    private boolean assignedToStat;

    public StatsSign(StatsAPI api, SignType type, Stat stat, String world, int x, int y, int z) {
        this.api = api;
        this.stat = stat;
        this.type = stat.getName();
        this.signType = type;
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public StatsSign(StatsAPI api, String location, ConfigurationSection section) {
        this.api = api;
        String[] split = location.split(",");
        world = split[0];
        x = Integer.parseInt(split[1]);
        y = Integer.parseInt(split[2]);
        z = Integer.parseInt(split[3]);
        signType = SignType.fromString(section.getString("type"));
        if (section.getString("dataType", null) == null) {
            stat = api.getStat(section.getString("stat"));
        } else {
            type = section.getString("dataType");
            if (type.equalsIgnoreCase("pvpkills")) {
                this.setVariable("SELECT " + (signType.equals(SignType.GLOBAL) ? "SUM(amount) " : "amount ")
                        + "FROM " + api.getDatabasePrefix() + "kill WHERE type='Player'"
                        + (signType.equals(SignType.GLOBAL) ? "" : "AND player_id='" + api.getPlayerId(section.getString("var")) + "'"));
                signType = SignType.CUSTOM;
                this.signLine = "PVP kills";
                type = "Kill";
                section.set("var", this.variable);
            } else if (type.equalsIgnoreCase("deaths") || type.equalsIgnoreCase("Kills")) {
                type = type.substring(0, type.length() - 2);
            } else if (type.equalsIgnoreCase("blocks broken")) {
                type = "Block break";
            } else if (type.equalsIgnoreCase("blocks placed")) {
                type = "Block place";
            }
            stat = api.getStat(type);
            section.set("dataType", null);
            section.set("stat", stat.getName());
        }
        this.signLine = section.getString("line", null);
        if (section.isSet("var")) {
            this.variable = section.getString("var");
            if (this.variable.contains("player=")) {
                this.variable = this.variable.replace("player=", "player_id=");
                String[] spl = variable.split("player_id=");
                String[] omg = spl[1].split(" ");
                if (omg.length == 0 || omg.length == 1) {
                    //you know what? Never mind :S
                    return;
                }
                this.variable = this.variable.replace(omg[0], "" + api.getPlayerId(omg[0]));
                section.set("var", this.variable);
            } else if (this.signType.equals(SignType.PLAYER)) {
                try {
                    Integer.parseInt(this.variable);
                } catch (NumberFormatException e) {
                    //it's a player name
                    this.variable = api.getPlayerId(this.variable) + "";
                    section.set("var", this.variable);
                }
            }
        }
    }

    public boolean isActive() {
        World w = Bukkit.getWorld(world);
        if (w == null) {
            return false;
        }
        if (!w.isChunkLoaded(x >> 4, z >> 4)) {
            return false;
        }
        Block b = w.getBlockAt(x, y, z);
        if (!b.getType().equals(Material.SIGN_POST) && !b.getType().equals(Material.WALL_SIGN)) {
            return false;
        }
        return b.hasMetadata("statssign");
    }

    public Stat getStat() {
        return stat;
    }

    public SignType getSignType() {
        return signType;
    }

    public void updateSign(String line1, String line2, String line3, String line4) {
        if (!isActive()) {
            return;
        }
        Sign s = (Sign) Bukkit.getWorld(world).getBlockAt(x, y, z).getState();
        s.setLine(0, line1);
        s.setLine(1, line2);
        s.setLine(2, line3);
        s.setLine(3, line4);
        s.update();
    }

    public String getLocationString() {
        return world + "," + x + "," + y + "," + z;
    }

    public String getWorld() {
        return world;
    }

    public void setVariable(String var) {
        this.variable = var;
    }

    public boolean hasVariable() {
        return this.variable != null;
    }

    public String getVariable() {
        return this.variable;
    }

    public String getSignLine() {
        if (this.signType.equals(SignType.CUSTOM)) {
            return signLine;
        }
        if (stat == null) {
            stat = api.getStat(type);
            if (stat == null) {
                return "Broken Stat :(";
            }
        }
        return stat.getName();
    }

    /*
     * Only used for <code>SignDataType.CUSTOM</code>
     */
    public void setSignLine(String signLine) {
        this.signLine = signLine;
    }

    public void setAttachedToStat(boolean b) {
        this.assignedToStat = b;
    }

    public boolean isAttachedToStat() {
        return this.assignedToStat;
    }
}
