/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.signs.events;

import nl.lolmewn.stats.signs.StatsSign;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class StatsSignDestroyEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled = false;
    private final StatsSign sign;
    private final Player player;

    public StatsSignDestroyEvent(Player player, StatsSign sign) {
        this.sign = sign;
        this.player = player;
    }

    public StatsSign getStatsSign() {
        return sign;
    }

    public Player getPlayer() {
        return player;
    }

    public Location getBlockLocation() {
        if (sign.isActive()) {
            String[] split = sign.getLocationString().split(",");
            Location loc = new Location(Bukkit.getWorld(split[0]),
                    Integer.parseInt(split[1]),
                    Integer.parseInt(split[2]),
                    Integer.parseInt(split[3]));
            return loc;
        }
        return null;
    }

    public Sign getSignBlock() {
        Location loc = this.getBlockLocation();
        if (loc == null) {
            return null;
        }
        return (Sign) loc.getBlock();
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
}
