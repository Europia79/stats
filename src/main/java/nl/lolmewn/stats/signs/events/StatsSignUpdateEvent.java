package nl.lolmewn.stats.signs.events;

import nl.lolmewn.stats.signs.StatsSign;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author Lolmewn
 */
public class StatsSignUpdateEvent extends Event implements Cancellable {

    private boolean cancelled = false;
    private static final HandlerList handlers = new HandlerList();
    private final StatsSign sign;
    private final String[] newLines;

    public StatsSignUpdateEvent(StatsSign sign, String[] newLines) {
        this.sign = sign;
        this.newLines = newLines;
    }

    public String[] getNewLines() {
        return newLines;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }

    public StatsSign getStatsSign() {
        return sign;
    }

    public Sign getSignBlock() {
        if (sign.isActive()) {
            String[] split = sign.getLocationString().split(",");
            Location loc = new Location(Bukkit.getWorld(split[0]),
                    Integer.parseInt(split[1]),
                    Integer.parseInt(split[2]),
                    Integer.parseInt(split[3]));
            return (Sign) loc.getBlock();
        } else {
            return null;
        }
    }

}
