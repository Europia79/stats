/**
 * MySQL Inherited subclass for making a connection to a MySQL server.
 *
 * Date Created: 2011-08-26 19:08
 *
 * @author PatPeter
 */
package nl.lolmewn.stats.mysql;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

public class MySQLLib extends Database {

    private PoolingDataSource source;

    public MySQLLib(Logger log,
            String prefix,
            String hostname,
            String portnmbr,
            String database,
            String username,
            String password) {
        super(log, prefix, "[MySQL] ");
        this.initialize();
        String url = "jdbc:mysql://" + hostname + ":" + portnmbr + "/" + database + "?zeroDateTimeBehavior=convertToNull";
        PoolableConnectionFactory factory = new PoolableConnectionFactory(
                new DriverManagerConnectionFactory(url, username, password),
                new GenericObjectPool(null, 15, GenericObjectPool.WHEN_EXHAUSTED_GROW, 1000, 10, true, true),
                null,
                "SELECT 1",
                false,
                true,
                Connection.TRANSACTION_SERIALIZABLE);
        source = new PoolingDataSource(factory.getPool());
    }

    @Override
    protected final boolean initialize() {
        try {
            Class.forName("com.mysql.jdbc.Driver"); // Check that server's Java has MySQL support.
            return true;
        } catch (ClassNotFoundException e) {
            this.writeError("Class not found in initialize(): " + e.getMessage() + ".", true);
            return false;
        }
    }

    @Override
    public synchronized Connection open() {
        if (initialize()) {
            try {
                return this.source.getConnection();
            } catch (SQLException ex) {
                log.severe("Couldn't retrieve connection from pool, erroring");
            }
        }
        return null;
    }

    @Override
    public Connection getConnection() {
        return this.open();
    }

    public void exit() {
        this.source = null;
    }
}
